﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using mg.pummelz;


namespace mg.pummelz.omega
{
    public class MGPumOmegaAIPlayerController : MGPumStudentAIPlayerController
    {

        public const string type = "OMEGA";

        public MGPumOmegaAIPlayerController(int playerID) : base(playerID)
        {
        }

        internal override MGPumCommand calculateCommand()
        {
            List<MGPumUnit> possibleMovers = new List<MGPumUnit>();

            foreach (MGPumUnit unit in state.getAllUnitsInZone(MGPumZoneType.Battlegrounds, this.playerID))
            {
                if (stateOracle.canMove(unit))
                {
                    if (unit.currentSpeed < 1)
                        continue;

                    MGPumMoveChainMatcher matcher = unit.getMoveMatcher();
                    MGPumFieldChain chain = new MGPumFieldChain(unit.ownerID, matcher);
                    chain.add(unit.field);

                    Vector2Int goalCoords = unit.field.coords + Vector2Int.up;
                    MGPumField goal = state.fields.getField(goalCoords);
                    if (state.fields.inBounds(goalCoords)) if (chain.canAdd(goal)) chain.add(goal);

                    if (chain.isValidChain())
                    {
                        //Debug.Log("chain valid");
                        MGPumMoveCommand mc = new MGPumMoveCommand(this.playerID, chain, unit);
                        if (stateOracle.checkMoveCommand(mc))
                        {
                            return mc;
                        }
                    }
                }
            }




            return new MGPumEndTurnCommand(this.playerID);
        }


        protected override int[] getTeamMartikels()
        {
            return new int[] { 2074465, 3983204 };
        }
    }
}